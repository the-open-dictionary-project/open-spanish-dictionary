package cymru.prv.dictionary.spanish

import cymru.prv.dictionary.common.Dictionary
import cymru.prv.dictionary.common.GrammaticalGender
import org.json.JSONObject
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

/**
 * @author Preben Vangberg
 */
class TestSpanishNoun {

    private fun testGender(normalForm: String, expected: String, gender: String? = null){
        val word = SpanishNoun(JSONObject()
            .put("normalForm", normalForm)
            .putOpt("gender", gender)
        )
        val expectedSet = GrammaticalGender.fromString(expected)
        Assertions.assertEquals(
            expectedSet,
            GrammaticalGender.fromString(word.toJson().getString("gender"))
        )
    }

    @Test
    fun `should be able to override gender`() =
        testGender("mano", "f","f")

    @Test
    fun `words ending with ista should be both genders`() =
        testGender("artista", "mf")

    @Test
    fun `words ending with sión should be feminine`() =
        testGender("televisión", "f")

    @Test
    fun `words ending with ción should be feminine`() =
        testGender("nación", "f")

}