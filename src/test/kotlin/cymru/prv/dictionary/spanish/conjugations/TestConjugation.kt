package cymru.prv.dictionary.spanish.conjugations

import cymru.prv.dictionary.common.Dictionary
import cymru.prv.dictionary.spanish.SpanishVerb
import org.json.JSONObject
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

/**
 * @author Preben Vangberg
 */
open class TestConjugation {

    internal fun testConjugationForm(
        normalForm: String,
        expectedClass: SpanishVerb.ConjugationClass,
        expectedForm: String,
        tense: String,
        form: String
    ){
        val verb = SpanishVerb(JSONObject().put("normalForm", normalForm))
        Assertions.assertEquals(expectedClass, verb.conjugationClass)
        Assertions.assertEquals(expectedForm, verb.conjugations.getJSONObject(tense).getJSONArray(form)[0])
    }

    @Test
    fun `should be able to set conjugation class manually`(){
        val verb = SpanishVerb(JSONObject()
            .put("normalForm", "estar")
            .put("class", "ir")
        )
        Assertions.assertEquals("estimos", verb
            .toJson()
            .getJSONObject("conjugations")
            .getJSONObject("present")
            .getJSONArray("plurFirst")[0]
        )
    }

}