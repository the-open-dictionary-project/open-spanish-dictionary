package cymru.prv.dictionary.spanish.conjugations

import cymru.prv.dictionary.common.Dictionary
import cymru.prv.dictionary.spanish.SpanishDictionary
import cymru.prv.dictionary.spanish.SpanishVerb
import org.json.JSONArray
import org.json.JSONObject
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

/**
 * @author Preben Vangberg
 */
class TestSpanishVerb {

    private fun testConjugation(normalForm: String, conjugation: String, expected: String){
        val verb = SpanishVerb(JSONObject()
            .put("normalForm", normalForm))
        Assertions.assertEquals(expected, verb.toJson()
            .getJSONObject("conjugations")
            .getJSONArray(conjugation)[0])
    }

    @Test
    fun `past participle of ar verbs should be stem + ado`() =
        testConjugation("estar", "pastParticiple", "estado")

    @Test
    fun `past participle of er verbs should be stem + ido`() =
        testConjugation("tener", "pastParticiple", "tenido")

    @Test
    fun `past participle of ir verbs should be stem + ido`() =
        testConjugation("vivir", "pastParticiple", "vivido")

    @Test
    fun `should not export past participle if empty`(){
        val verb = SpanishVerb(JSONObject()
            .put("normalForm", "test")
            .put("pastParticiple", JSONArray()))

        Assertions.assertFalse(
            verb.toJson().getJSONObject("conjugations")
                .has("pastParticiple")
        )
    }


    @Test
    fun `gerund of ar verbs should be stem + ando`() =
        testConjugation("estar", "gerund", "estando")

    @Test
    fun `gerund of er verbs should be stem + iendo`() =
        testConjugation("tener", "gerund", "teniendo")

    @Test
    fun `gerund of ir verbs should be stem + iendo`() =
        testConjugation("vivir", "gerund", "viviendo")

    @Test
    fun `should not export gerund if empty`(){
        val verb = SpanishVerb(JSONObject()
            .put("normalForm", "test")
            .put("gerund", JSONArray()))

        Assertions.assertFalse(
            verb.toJson().getJSONObject("conjugations")
                .has("gerund")
        )
    }


}