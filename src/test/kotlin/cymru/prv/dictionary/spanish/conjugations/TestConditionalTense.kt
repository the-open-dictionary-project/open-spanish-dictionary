package cymru.prv.dictionary.spanish.conjugations

import cymru.prv.dictionary.common.Dictionary
import cymru.prv.dictionary.spanish.SpanishVerb
import cymru.prv.dictionary.spanish.SpanishVerb.ConjugationClass
import org.json.JSONObject
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

/**
 * Tests that the conditional tense is exported correctly
 * and that the verbs are conjugated correctly based on
 * their verb class.
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
class TestConditionalTense: TestConjugation() {

    /**
     * Shortened version of testConjugationForm
     * with conditional tense already inserted
     */
    private fun testExpected(
        normalForm: String,
        expectedClass: ConjugationClass,
        expectedForm: String,
        form: String
    ) = testConjugationForm(normalForm, expectedClass, expectedForm, "conditional", form)

    @Test
    fun `should be able to override class and stem on tense level`(){
        val verb = SpanishVerb(JSONObject()
            .put("normalForm", "hacer")
            .put("conditional", JSONObject()
                .put("class", "ar")
                .put("stem", "h")
            )
        )
        val present = verb.conjugations.getJSONObject("conditional")
        assertEquals("haría", present.getJSONArray("singFirst")[0])
        assertEquals("harías", present.getJSONArray("singSecond")[0])
        assertEquals("haría", present.getJSONArray("singThird")[0])
        assertEquals("haríamos", present.getJSONArray("plurFirst")[0])
        assertEquals("haríais", present.getJSONArray("plurSecond")[0])
        assertEquals("harían", present.getJSONArray("plurThird")[0])
    }

    @Test
    fun `should export tense properly`(){
        val verb = SpanishVerb(JSONObject()
            .put("normalForm", "ser")
        )
        assertTrue(verb.conjugations.has("conditional"))
    }

    @Test
    fun `should not export if has flag is not set`(){
        val verb = SpanishVerb(JSONObject()
            .put("normalForm", "ser")
            .put("conditional", JSONObject()
                .put("has", false)
            )
        )
        assertFalse(verb.conjugations.has("conditional"))
    }

    /* Overrides */
    @Test
    fun `test verb overrides`(){
        val verb = SpanishVerb(JSONObject()
            .put("normalForm", "hacer")
            .put("conditional", JSONObject()
                .put("singFirst", "haría")
                .put("singSecond", "harías")
                .put("singThird", "haría")
                .put("plurFirst", "haríamos")
                .put("plurSecond", "haríais")
                .put("plurThird", "harían")
            )
        )
        val present = verb.conjugations.getJSONObject("conditional")
        assertEquals("haría", present.getJSONArray("singFirst")[0])
        assertEquals("harías", present.getJSONArray("singSecond")[0])
        assertEquals("haría", present.getJSONArray("singThird")[0])
        assertEquals("haríamos", present.getJSONArray("plurFirst")[0])
        assertEquals("haríais", present.getJSONArray("plurSecond")[0])
        assertEquals("harían", present.getJSONArray("plurThird")[0])
    }

    /* Singular First */
    @Test
    fun `ar-class words should return stem +aría for sing first`() =
        testExpected("amar", ConjugationClass.AR, "amaría", "singFirst")

    @Test
    fun `er-class words should return stem +ería for sing first`() =
        testExpected("temer", ConjugationClass.ER, "temería", "singFirst")

    @Test
    fun `ir-class words should return stem +iría for sing first`() =
        testExpected("partir", ConjugationClass.IR, "partiría", "singFirst")


    /* Singular Second */
    @Test
    fun `ar-class words should return stem +arías for singular second`() =
        testExpected("amar", ConjugationClass.AR, "amarías", "singSecond")

    @Test
    fun `er-class words should return stem +erías for singular second`() =
        testExpected("temer", ConjugationClass.ER, "temerías", "singSecond")

    @Test
    fun `ir-class words should return stem +irías for singular second`() =
        testExpected("partir", ConjugationClass.IR, "partirías", "singSecond")


    /* Singular Third */
    @Test
    fun `ar-class words should return stem +aría for singular third`() =
        testExpected("amar", ConjugationClass.AR, "amaría", "singThird")

    @Test
    fun `er-class words should return stem +ería for singular third`() =
        testExpected("temer", ConjugationClass.ER, "temería", "singThird")

    @Test
    fun `ir-class words should return stem +iría for singular third`() =
        testExpected("partir", ConjugationClass.IR, "partiría", "singThird")


    /* Plural First */
    @Test
    fun `ar-class words should return stem +aríamos for plural first`() =
        testExpected("amar", ConjugationClass.AR, "amaríamos", "plurFirst")

    @Test
    fun `er-class words should return stem +eríamos for plural first`() =
        testExpected("temer", ConjugationClass.ER, "temeríamos", "plurFirst")

    @Test
    fun `ir-class words should return stem +iríamos for plural first`() =
        testExpected("vivir", ConjugationClass.IR, "viviríamos", "plurFirst")


    /* Plural Second */
    @Test
    fun `ar-class words should return stem +aríais for plural second`() =
        testExpected("amar", ConjugationClass.AR, "amaríais", "plurSecond")

    @Test
    fun `er-class words should return stem +eríais for plural second`() =
        testExpected("temer", ConjugationClass.ER, "temeríais", "plurSecond")

    @Test
    fun `ir-class words should return stem +iríais for plural second`() =
        testExpected("vivir", ConjugationClass.IR, "viviríais", "plurSecond")


    /* Plural Third */
    @Test
    fun `ar-class words should return stem +arían for plural third`() =
        testExpected("amar", ConjugationClass.AR, "amarían", "plurThird")

    @Test
    fun `er-class words should return stem +erían for plural third`() =
        testExpected("temer", ConjugationClass.ER, "temerían", "plurThird")

    @Test
    fun `ir-class words should return stem +irían for plural third`() =
        testExpected("vivir", ConjugationClass.IR, "vivirían", "plurThird")
}