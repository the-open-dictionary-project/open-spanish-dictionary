package cymru.prv.dictionary.spanish.conjugations

import cymru.prv.dictionary.common.Dictionary
import cymru.prv.dictionary.spanish.SpanishVerb
import cymru.prv.dictionary.spanish.SpanishVerb.ConjugationClass
import org.json.JSONObject
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Test

/**
 * Tests that the present tense is exported correctly
 * and that the verbs are conjugated correctly based on
 * their verb class.
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
class TestPresentTense: TestConjugation() {

    /**
     * Shortened version of testConjugationForm
     * with present tense already inserted
     */
    private fun testExpected(
        normalForm: String,
        expectedClass: ConjugationClass,
        expectedForm: String,
        form: String
    ) = testConjugationForm(normalForm, expectedClass, expectedForm, "present", form)

    @Test
    fun `should not export if has flag is not set`(){
        val verb = SpanishVerb(JSONObject()
            .put("normalForm", "ser")
            .put("present", JSONObject()
                .put("has", false)
            )
        )
        assertFalse(verb.conjugations.has("present"))
    }

    /* Overrides */
    @Test
    fun `test verb overrides`(){
        val verb = SpanishVerb(JSONObject()
            .put("normalForm", "ser")
            .put("present", JSONObject()
                .put("singFirst", "soy")
                .put("singSecond", "eres")
                .put("singThird", "es")
                .put("plurFirst", "somos")
                .put("plurSecond", "sois")
                .put("plurThird", "son")
            )
        )
        val present = verb.conjugations.getJSONObject("present")
        assertEquals("soy", present.getJSONArray("singFirst")[0])
        assertEquals("eres", present.getJSONArray("singSecond")[0])
        assertEquals("es", present.getJSONArray("singThird")[0])
        assertEquals("somos", present.getJSONArray("plurFirst")[0])
        assertEquals("sois", present.getJSONArray("plurSecond")[0])
        assertEquals("son", present.getJSONArray("plurThird")[0])
    }

    @Test
    fun `test alt stem for certain forms`(){
        val verb = SpanishVerb(JSONObject()
            .put("normalForm", "cerrar")
            .put("present", JSONObject()
                .put("altStem", "cierr")
            )
        )
        val present = verb.conjugations.getJSONObject("present")
        assertEquals("cierro", present.getJSONArray("singFirst")[0])
        assertEquals("cierras", present.getJSONArray("singSecond")[0])
        assertEquals("cierra", present.getJSONArray("singThird")[0])
        assertEquals("cerramos", present.getJSONArray("plurFirst")[0])
        assertEquals("cerráis", present.getJSONArray("plurSecond")[0])
        assertEquals("cierran", present.getJSONArray("plurThird")[0])
    }

    /* Singular First */
    @Test
    fun `ar-class words should return stem +o for sing first`() =
        testExpected("amar", ConjugationClass.AR, "amo", "singFirst")

    @Test
    fun `er-class words should return stem +o for sing first`() =
        testExpected("temer", ConjugationClass.ER, "temo", "singFirst")

    @Test
    fun `ir-class words should return stem +o for sing first`() =
        testExpected("partir", ConjugationClass.IR, "parto", "singFirst")


    /* Singular Second */
    @Test
    fun `ar-class words should return stem +as for singular second`() =
        testExpected("amar", ConjugationClass.AR, "amas", "singSecond")

    @Test
    fun `er-class words should return stem +es for singular second`() =
        testExpected("temer", ConjugationClass.ER, "temes", "singSecond")

    @Test
    fun `ir-class words should return stem +es for singular second`() =
        testExpected("partir", ConjugationClass.IR, "partes", "singSecond")


    /* Singular Third */
    @Test
    fun `ar-class words should return stem +a for singular third`() =
        testExpected("amar", ConjugationClass.AR, "ama", "singThird")

    @Test
    fun `er-class words should return stem +e for singular third`() =
        testExpected("temer", ConjugationClass.ER, "teme", "singThird")

    @Test
    fun `ir-class words should return stem +e for singular third`() =
        testExpected("partir", ConjugationClass.IR, "parte", "singThird")


    /* Plural First */
    @Test
    fun `ar-class words should return stem +amos for plural first`() =
        testExpected("amar", ConjugationClass.AR, "amamos", "plurFirst")

    @Test
    fun `er-class words should return stem +emos for plural first`() =
        testExpected("temer", ConjugationClass.ER, "tememos", "plurFirst")

    @Test
    fun `ir-class words should return stem +imos for plural first`() =
        testExpected("vivir", ConjugationClass.IR, "vivimos", "plurFirst")


    /* Plural Second */
    @Test
    fun `ar-class words should return stem +áis for plural second`() =
        testExpected("amar", ConjugationClass.AR, "amáis", "plurSecond")

    @Test
    fun `er-class words should return stem +éis for plural second`() =
        testExpected("temer", ConjugationClass.ER, "teméis", "plurSecond")

    @Test
    fun `ir-class words should return stem +ís for plural second`() =
        testExpected("vivir", ConjugationClass.IR, "vivís", "plurSecond")


    /* Plural Third */
    @Test
    fun `ar-class words should return stem +an for plural third`() =
        testExpected("amar", ConjugationClass.AR, "aman", "plurThird")

    @Test
    fun `er-class words should return stem +en for plural third`() =
        testExpected("temer", ConjugationClass.ER, "temen", "plurThird")

    @Test
    fun `ir-class words should return stem +en for plural third`() =
        testExpected("vivir", ConjugationClass.IR, "viven", "plurThird")
}