package cymru.prv.dictionary.spanish.conjugations

import cymru.prv.dictionary.common.Dictionary
import cymru.prv.dictionary.spanish.SpanishVerb
import cymru.prv.dictionary.spanish.SpanishVerb.ConjugationClass
import org.json.JSONObject
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

/**
 * Tests that the future tense is exported correctly
 * and that the verbs are conjugated correctly based on
 * their verb class.
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
class TestFutureTense: TestConjugation() {

    /**
     * Shortened version of testConjugationForm
     * with future tense already inserted
     */
    private fun testExpected(
        normalForm: String,
        expectedClass: ConjugationClass,
        expectedForm: String,
        form: String
    ) = testConjugationForm(normalForm, expectedClass, expectedForm, "future", form)

    @Test
    fun `should be able to override class and stem on tense level`(){
        val verb = SpanishVerb(JSONObject()
            .put("normalForm", "hacer")
            .put("future", JSONObject()
                .put("class", "ar")
                .put("stem", "h")
            )
        )
        val present = verb.conjugations.getJSONObject("future")
        assertEquals("haré", present.getJSONArray("singFirst")[0])
        assertEquals("harás", present.getJSONArray("singSecond")[0])
        assertEquals("hará", present.getJSONArray("singThird")[0])
        assertEquals("haremos", present.getJSONArray("plurFirst")[0])
        assertEquals("haréis", present.getJSONArray("plurSecond")[0])
        assertEquals("harán", present.getJSONArray("plurThird")[0])
    }

    @Test
    fun `should export tense properly`(){
        val verb = SpanishVerb(JSONObject()
            .put("normalForm", "ser")
        )
        assertTrue(verb.conjugations.has("future"))
    }

    @Test
    fun `should not export if has flag is not set`(){
        val verb = SpanishVerb(JSONObject()
            .put("normalForm", "ser")
            .put("future", JSONObject()
                .put("has", false)
            )
        )
        assertFalse(verb.conjugations.has("future"))
    }

    /* Overrides */
    @Test
    fun `test verb overrides`(){
        val verb = SpanishVerb(JSONObject()
            .put("normalForm", "hacer")
            .put("future", JSONObject()
                .put("singFirst", "haré")
                .put("singSecond", "harás")
                .put("singThird", "hará")
                .put("plurFirst", "haremos")
                .put("plurSecond", "haréis")
                .put("plurThird", "harán")
            )
        )
        val present = verb.conjugations.getJSONObject("future")
        assertEquals("haré", present.getJSONArray("singFirst")[0])
        assertEquals("harás", present.getJSONArray("singSecond")[0])
        assertEquals("hará", present.getJSONArray("singThird")[0])
        assertEquals("haremos", present.getJSONArray("plurFirst")[0])
        assertEquals("haréis", present.getJSONArray("plurSecond")[0])
        assertEquals("harán", present.getJSONArray("plurThird")[0])
    }

    /* Singular First */
    @Test
    fun `ar-class words should return stem +aré for sing first`() =
        testExpected("amar", ConjugationClass.AR, "amaré", "singFirst")

    @Test
    fun `er-class words should return stem +eré for sing first`() =
        testExpected("temer", ConjugationClass.ER, "temeré", "singFirst")

    @Test
    fun `ir-class words should return stem +iré for sing first`() =
        testExpected("partir", ConjugationClass.IR, "partiré", "singFirst")


    /* Singular Second */
    @Test
    fun `ar-class words should return stem +arás for singular second`() =
        testExpected("amar", ConjugationClass.AR, "amarás", "singSecond")

    @Test
    fun `er-class words should return stem +erás for singular second`() =
        testExpected("temer", ConjugationClass.ER, "temerás", "singSecond")

    @Test
    fun `ir-class words should return stem +irás for singular second`() =
        testExpected("partir", ConjugationClass.IR, "partirás", "singSecond")


    /* Singular Third */
    @Test
    fun `ar-class words should return stem +ará for singular third`() =
        testExpected("amar", ConjugationClass.AR, "amará", "singThird")

    @Test
    fun `er-class words should return stem +erá for singular third`() =
        testExpected("temer", ConjugationClass.ER, "temerá", "singThird")

    @Test
    fun `ir-class words should return stem +irá for singular third`() =
        testExpected("partir", ConjugationClass.IR, "partirá", "singThird")


    /* Plural First */
    @Test
    fun `ar-class words should return stem +aremos for plural first`() =
        testExpected("amar", ConjugationClass.AR, "amaremos", "plurFirst")

    @Test
    fun `er-class words should return stem +eremos for plural first`() =
        testExpected("temer", ConjugationClass.ER, "temeremos", "plurFirst")

    @Test
    fun `ir-class words should return stem +iremos for plural first`() =
        testExpected("vivir", ConjugationClass.IR, "viviremos", "plurFirst")


    /* Plural Second */
    @Test
    fun `ar-class words should return stem +aréis for plural second`() =
        testExpected("amar", ConjugationClass.AR, "amaréis", "plurSecond")

    @Test
    fun `er-class words should return stem +eréis for plural second`() =
        testExpected("temer", ConjugationClass.ER, "temeréis", "plurSecond")

    @Test
    fun `ir-class words should return stem +iréis for plural second`() =
        testExpected("vivir", ConjugationClass.IR, "viviréis", "plurSecond")


    /* Plural Third */
    @Test
    fun `ar-class words should return stem +arán for plural third`() =
        testExpected("amar", ConjugationClass.AR, "amarán", "plurThird")

    @Test
    fun `er-class words should return stem +erán for plural third`() =
        testExpected("temer", ConjugationClass.ER, "temerán", "plurThird")

    @Test
    fun `ir-class words should return stem +irán for plural third`() =
        testExpected("vivir", ConjugationClass.IR, "vivirán", "plurThird")
}