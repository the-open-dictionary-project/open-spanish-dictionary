package cymru.prv.dictionary.spanish

import cymru.prv.dictionary.common.Dictionary
import cymru.prv.dictionary.common.DictionaryList
import cymru.prv.dictionary.common.Word
import cymru.prv.dictionary.common.WordType
import org.json.JSONObject
import java.util.function.BiFunction
import java.util.function.Function

class SpanishDictionary(list: DictionaryList = DictionaryList()) : Dictionary (
    list,
    "es",
    mutableMapOf<WordType, Function<JSONObject, Word>>(
        WordType.noun to Function { j -> SpanishNoun(j)},
        WordType.verb to Function { j -> SpanishVerb(j)},
    )
){

    @Override
    override fun getVersion(): String {
        return "1.2.0"
    }

}
