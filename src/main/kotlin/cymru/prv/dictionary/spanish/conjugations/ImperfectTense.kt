package cymru.prv.dictionary.spanish.conjugations

import cymru.prv.dictionary.spanish.SpanishVerb
import cymru.prv.dictionary.spanish.SpanishVerb.ConjugationClass
import org.json.JSONObject

/**
 * @author Preben Vangberg
 */
class ImperfectTense(obj: JSONObject, verb: SpanishVerb): SpanishConjugation(obj, verb) {

    override fun getDefaultSingularFirst(): MutableList<String> {
        return when(conjugationClass){
            ConjugationClass.AR -> mutableListOf(stem + "aba")
            else -> mutableListOf(stem + "ía")
        }
    }

    override fun getDefaultSingularSecond(): MutableList<String> {
        return when(conjugationClass){
            ConjugationClass.AR -> mutableListOf(stem + "abas")
            else -> mutableListOf(stem + "ías")
        }
    }

    override fun getDefaultSingularThird(): MutableList<String> {
        return when(conjugationClass){
            ConjugationClass.AR -> mutableListOf(stem + "aba")
            else -> mutableListOf(stem + "ía")
        }
    }

    override fun getDefaultPluralFirst(): MutableList<String> {
        return when(conjugationClass){
            ConjugationClass.AR -> mutableListOf(stem + "ábamos")
            else -> mutableListOf(stem + "íamos")
        }
    }

    override fun getDefaultPluralSecond(): MutableList<String> {
        return when(conjugationClass){
            ConjugationClass.AR -> mutableListOf(stem + "abais")
            else -> mutableListOf(stem + "íais")
        }
    }

    override fun getDefaultPluralThird(): MutableList<String> {
        return when(conjugationClass){
            ConjugationClass.AR -> mutableListOf(stem + "aban")
            else -> mutableListOf(stem + "ían")
        }
    }
}