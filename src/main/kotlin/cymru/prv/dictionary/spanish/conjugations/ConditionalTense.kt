package cymru.prv.dictionary.spanish.conjugations

import cymru.prv.dictionary.spanish.SpanishVerb
import org.json.JSONObject

/**
 * @author Preben Vangberg
 */
class ConditionalTense(obj: JSONObject, verb: SpanishVerb): SpanishConjugationWithShortForm(obj, verb) {

    override fun getDefaultSingularFirst(): MutableList<String> {
        return when(conjugationClass){
            SpanishVerb.ConjugationClass.AR -> mutableListOf(stem + form("aría"))
            SpanishVerb.ConjugationClass.ER -> mutableListOf(stem + form("ería"))
            SpanishVerb.ConjugationClass.IR -> mutableListOf(stem + form("iría"))
        }
    }

    override fun getDefaultSingularSecond(): MutableList<String> {
        return when(conjugationClass){
            SpanishVerb.ConjugationClass.AR -> mutableListOf(stem + form("arías"))
            SpanishVerb.ConjugationClass.ER -> mutableListOf(stem + form("erías"))
            SpanishVerb.ConjugationClass.IR -> mutableListOf(stem + form("irías"))
        }
    }

    override fun getDefaultSingularThird(): MutableList<String> {
        return when(conjugationClass){
            SpanishVerb.ConjugationClass.AR -> mutableListOf(stem + form("aría"))
            SpanishVerb.ConjugationClass.ER -> mutableListOf(stem + form("ería"))
            SpanishVerb.ConjugationClass.IR -> mutableListOf(stem + form("iría"))
        }
    }

    override fun getDefaultPluralFirst(): MutableList<String> {
        return when(conjugationClass){
            SpanishVerb.ConjugationClass.AR -> mutableListOf(stem + form("aríamos"))
            SpanishVerb.ConjugationClass.ER -> mutableListOf(stem + form("eríamos"))
            SpanishVerb.ConjugationClass.IR -> mutableListOf(stem + form("iríamos"))
        }
    }

    override fun getDefaultPluralSecond(): MutableList<String> {
        return when(conjugationClass){
            SpanishVerb.ConjugationClass.AR -> mutableListOf(stem + form("aríais"))
            SpanishVerb.ConjugationClass.ER -> mutableListOf(stem + form("eríais"))
            SpanishVerb.ConjugationClass.IR -> mutableListOf(stem + form("iríais"))
        }
    }

    override fun getDefaultPluralThird(): MutableList<String> {
        return when(conjugationClass){
            SpanishVerb.ConjugationClass.AR -> mutableListOf(stem + form("arían"))
            SpanishVerb.ConjugationClass.ER -> mutableListOf(stem + form("erían"))
            SpanishVerb.ConjugationClass.IR -> mutableListOf(stem + form("irían"))
        }
    }
}