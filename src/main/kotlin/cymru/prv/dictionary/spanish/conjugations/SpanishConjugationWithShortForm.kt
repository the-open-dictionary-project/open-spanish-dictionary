package cymru.prv.dictionary.spanish.conjugations

import cymru.prv.dictionary.spanish.SpanishVerb
import org.json.JSONObject

/**
 * @author Preben Vangberg
 */
abstract class SpanishConjugationWithShortForm(obj: JSONObject, verb: SpanishVerb):
    SpanishConjugation(obj, verb)
{

    val shortForm = obj.optBoolean("shortForm")

    protected fun form(suffix: String): String {
        if(shortForm)
            return suffix.drop(2)
        return suffix
    }

}