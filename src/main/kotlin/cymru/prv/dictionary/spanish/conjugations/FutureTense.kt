package cymru.prv.dictionary.spanish.conjugations

import cymru.prv.dictionary.spanish.SpanishVerb
import cymru.prv.dictionary.spanish.SpanishVerb.ConjugationClass

import org.json.JSONObject

/**
 * Indicative future tense for Spanish verbs
 * @author Preben Vangberg
 * @since 1.0.0
 */
class FutureTense(obj: JSONObject, verb: SpanishVerb): SpanishConjugationWithShortForm(obj, verb) {

    override fun getDefaultSingularFirst(): MutableList<String> {
        return when(conjugationClass){
            ConjugationClass.AR -> mutableListOf(stem + form("aré"))
            ConjugationClass.ER -> mutableListOf(stem + form("eré"))
            ConjugationClass.IR -> mutableListOf(stem + form("iré"))
        }
    }

    override fun getDefaultSingularSecond(): MutableList<String> {
        return when(conjugationClass){
            ConjugationClass.AR -> mutableListOf(stem + form("arás"))
            ConjugationClass.ER -> mutableListOf(stem + form("erás"))
            ConjugationClass.IR -> mutableListOf(stem + form("irás"))
        }
    }

    override fun getDefaultSingularThird(): MutableList<String> {
        return when(conjugationClass){
            ConjugationClass.AR -> mutableListOf(stem + form("ará"))
            ConjugationClass.ER -> mutableListOf(stem + form("erá"))
            ConjugationClass.IR -> mutableListOf(stem + form("irá"))
        }
    }

    override fun getDefaultPluralFirst(): MutableList<String> {
        return when(conjugationClass){
            ConjugationClass.AR -> mutableListOf(stem + form("aremos"))
            ConjugationClass.ER -> mutableListOf(stem + form("eremos"))
            ConjugationClass.IR -> mutableListOf(stem + form("iremos"))
        }
    }

    override fun getDefaultPluralSecond(): MutableList<String> {
        return when(conjugationClass){
            ConjugationClass.AR -> mutableListOf(stem + form("aréis"))
            ConjugationClass.ER -> mutableListOf(stem + form("eréis"))
            ConjugationClass.IR -> mutableListOf(stem + form("iréis"))
        }
    }

    override fun getDefaultPluralThird(): MutableList<String> {
        return when(conjugationClass){
            ConjugationClass.AR -> mutableListOf(stem + form("arán"))
            ConjugationClass.ER -> mutableListOf(stem + form("erán"))
            ConjugationClass.IR -> mutableListOf(stem + form("irán"))
        }
    }
}