package cymru.prv.dictionary.spanish

import cymru.prv.dictionary.common.Conjugation
import cymru.prv.dictionary.common.Dictionary
import cymru.prv.dictionary.common.Word
import cymru.prv.dictionary.common.WordType
import cymru.prv.dictionary.common.json.Json
import cymru.prv.dictionary.spanish.conjugations.*
import org.json.JSONObject

/**
 * Represents a single Spanish verb.
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
class SpanishVerb(obj: JSONObject): Word(obj, WordType.verb) {

    companion object {
        val availableTenses = mapOf<String, (JSONObject, SpanishVerb) -> Conjugation>(
            "present" to { obj, verb -> PresentTense(obj, verb) },
            "preterite" to { obj, verb -> PreteriteTense(obj, verb) },
            "imperfect" to { obj, verb -> ImperfectTense(obj, verb) },
            "future" to { obj, verb -> FutureTense(obj, verb) },
            "conditional" to { obj, verb -> ConditionalTense(obj, verb) }
        )
    }

    enum class ConjugationClass {
        ER, AR, IR
    }
    val conjugationClass: ConjugationClass = when {
        obj.has("class") -> ConjugationClass.valueOf(obj.getString("class").uppercase())
        normalForm.endsWith("er") -> ConjugationClass.ER
        normalForm.endsWith("ir") -> ConjugationClass.IR
        else -> ConjugationClass.AR
    }

    val stem: String = obj.optString("stem", normalForm.replace("[aei]r$".toRegex(), ""))
    private val tenses: Map<String, Conjugation> = availableTenses
        .filter { (field, _) -> obj.has(field) }
        .map { (field, constructor) -> field to constructor(obj.getJSONObject(field), this) }
        .toMap()

    private val pastParticiple: List<String> = when {
        obj.has("pastParticiple") -> Json.getStringList(obj, "pastParticiple")
        conjugationClass == ConjugationClass.AR -> listOf(stem + "ado")
        else -> listOf(stem + "ido")
    }

    private val gerund: List<String> = when {
        obj.has("gerund") -> Json.getStringList(obj, "gerund")
        conjugationClass == ConjugationClass.AR -> listOf(stem + "ando")
        else -> listOf(stem + "iendo")
    }


    private fun getAllTenses() = sequence {
        availableTenses.forEach { (tense, constructor) ->
            if(tenses.containsKey(tense)) {
                val tenseOverride = tenses.getValue(tense)
                if (tenseOverride.has())
                    yield(tense to tenseOverride)
            }
            else
                yield(tense to constructor(JSONObject(), this@SpanishVerb))
        }
    }

    public override fun getConjugations(): JSONObject {
        val obj = JSONObject()
        getAllTenses().map { (tenseName, tense) ->
            tenseName to tense.toJson()
        }.forEach { obj.put(it.first, it.second) }

        if(pastParticiple.isNotEmpty())
            obj.put("pastParticiple", pastParticiple)
        if(gerund.isNotEmpty())
            obj.put("gerund", gerund)

        return obj
    }

    override fun getVersions(): MutableList<String> {
        val versions = super.getVersions()
        getAllTenses()
            .map { it.second.versions }
            .forEach { versions.addAll(it) }
        return versions
    }

}